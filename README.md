# Fasttrack
Easily and quickly make a GitLab project in the command line

## Why not GitHub?
Because GitHub is owned by Microsoft and is closed source. Why should I host my open source software on a closed source platform?

## Can you support X git hosting platform?
No, this script was made for the convienience of me so if you want GitHub/BitBucket/X git service support, clone it to your favourite git platform and edit the source code or write a new tool to work for you.

## Why use Ruby?
Because it was useful in writing qdfm and also the Gitlab crate for Rust is too poorly documented for me to have any idea on how to use it.
