require 'gitlab'

endpoint = "https://gitlab.com/api/v4"
token = "PUT TOKEN HERE!"

client = Gitlab.client(endpoint: endpoint, private_token: token)

if ARGV.length != 2
	puts "Error: We need exactly two arguments!"
	puts "Usage: ruby fasttrack.rb {project name} {description}"
	exit
end

name = ARGV[0]
desc = ARGV[1]

project = client.create_project(name, {description: desc, visibility: "public"}) 
puts "Success: Created GitLab project #{ARGV[0]}."
puts "Push an existing Git repository."
puts "git remote add origin git@[DOMAIN]:[USERNAME]/#{ARGV[0]}.git"
puts "git push -u origin --all"
puts "git push -u origin --tags"
puts ""
puts "Message: Thank you for using Fasttrack!"
puts "https://gitlab.com/threeoh6000/fasttrack"
